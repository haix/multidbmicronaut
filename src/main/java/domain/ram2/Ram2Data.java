package domain.ram2;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "ram2")
public class Ram2Data {
    public Ram2Data() {}

    public Ram2Data( @NotNull String name){
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
