package multi.db;

import domain.ram1.Ram1Data;
import domain.ram2.Ram2Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

public interface Repository {
    Optional<Ram1Data> findById1(@NotNull Long id);
    Optional<Ram2Data> findById2(@NotNull Long id);

    Ram1Data save1(@NotBlank String name);
    Ram2Data save2(@NotBlank String name);

    List<Ram1Data> findAll1();
    List<Ram2Data> findAll2();
}
