package multi.db;

import domain.ram1.Ram1Data;
import domain.ram2.Ram2Data;
import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession;
import io.micronaut.spring.tx.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

public class RepositoryImpl implements Repository {

    @PersistenceContext
    private EntityManager entityManager1;
    @PersistenceContext(name = "ram2")
    private EntityManager entityManager2;

    public RepositoryImpl(@CurrentSession EntityManager entityManager1,
                          @CurrentSession("ram2") EntityManager entityManager2 ){
        this.entityManager1 = entityManager1;
        this.entityManager2 = entityManager2;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Ram1Data> findById1(@NotNull Long id) {
        return Optional.ofNullable(entityManager1.find(Ram1Data.class, id));
    }

    @Override
    @Transactional(readOnly = true, value = "ram2")
    public Optional<Ram2Data> findById2(@NotNull Long id) {
        return Optional.ofNullable(entityManager2.find(Ram2Data.class, id));
    }

    @Override
    @Transactional()
    public Ram1Data save1(@NotBlank String name) {
        Ram1Data d = new Ram1Data(name);
        entityManager1.persist(d);
        return d;
    }

    @Override
    @Transactional("ram2")
    public Ram2Data save2(@NotBlank String name) {
        Ram2Data d = new Ram2Data(name);
        entityManager2.persist(d);
        return d;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Ram1Data> findAll1() {
        String qs = "SELECT d FROM ram1 AS d";
        TypedQuery<Ram1Data> query = entityManager1.createQuery(qs, Ram1Data.class);

        return query.getResultList();
    }

    @Override
    @Transactional(readOnly = true, value = "ram2")
    public List<Ram2Data> findAll2() {
        String qs = "SELECT d FROM ram2 AS d";
        TypedQuery<Ram2Data> query = entityManager2.createQuery(qs, Ram2Data.class);

        return query.getResultList();
    }
}
