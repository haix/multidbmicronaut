package multi.db;

import domain.ram1.Ram1Data;
import domain.ram2.Ram2Data;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.validation.Validated;

import java.util.List;

@Validated
@Controller("/ram")
public class RamController {
    protected final Repository ramRep;

    public RamController(Repository ramRep){
        this.ramRep = ramRep;
    }

    @Get("/init")
    public HttpResponse init(){
        for(int i = 1; i < 5; i++){
            ramRep.save2("RAM2-data <" + i + ">");
            ramRep.save1("RAM1-data <" + i + ">");
        }
        return HttpResponse.noContent();
    }

    @Get("/1/{id}")
    public Ram1Data show1(Long id){
        return ramRep.findById1(id).orElse(null);
    }
    @Get("/2/{id}")
    public Ram2Data show2(Long id){
        return ramRep.findById2(id).orElse(null);
    }


    @Get(value = "/1/list")
    public List<Ram1Data> list1(){
        return ramRep.findAll1();
    }
    @Get(value = "/2/list")
    public List<Ram2Data> list2(){
        return ramRep.findAll2();
    }
}
